package com.eng.imdbcloneapp

import android.graphics.Bitmap
import android.widget.ImageView
import android.widget.TextView
import org.json.JSONObject
import java.io.Serializable

object DataGlobal {

    var moviesList: ArrayList<Movies> = arrayListOf()
    var imagesForSlide: ArrayList<Banner> = arrayListOf()
    val delayMillis = 2500L
    val jsonUrl = "https://imdb-api.com/en/API/MostPopularMovies/k_bs7tf9gu"
    val imageList: ArrayList<Bitmap> = arrayListOf()

    fun addImagesForSlider() {

        imagesForSlide.add(
            Banner(
                "Conjuring:The Devil Made Me Do It ",
                R.drawable.conjuring,
                R.drawable.conjuringsmall
            )
        )
        imagesForSlide.add(Banner("Nic Cage stars in Pig", R.drawable.pig, R.drawable.pigsmall))
        imagesForSlide.add(
            Banner(
                "Prequel film about young Cruella de Vil",
                R.drawable.cruella, R.drawable.cruellasmall
            )
        )
        imagesForSlide.add(
            Banner(
                "The Tomorrow War:Watch the final trailer",
                R.drawable.tommorowwar, R.drawable.tomorrowwarsmall
            )
        )
        imagesForSlide.add(
            Banner(
                "Space Jam: A New Legacy",
                R.drawable.spacejam,
                R.drawable.spacejamsmall
            )
        )

    }


}

class Login
    (
    val user: String = "user",
    val password: String = "user@user.com"
)

class Banner(
    val title: String,
    val imageBig: Int,
    val imageSmall: Int
)

class Movies(
    var id: String = "",
    var rank: String = "",
    var rankUpDown: String = "",
    var title: String = "",
    var fullTitle: String = "",
    var year: String = "",
    var image: String = "",
    var crew: String = "",
    var imdbRating: String = "",
    var imdbRatingCount: String = ""


)


