package com.eng.imdbcloneapp

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.json.JSONObject
import java.io.DataInput
import java.io.Serializable
import java.net.HttpURLConnection
import java.net.URL
import kotlin.math.abs


class MainActivity : AppCompatActivity() {

    var onpause = false
    var imageCounter = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DataGlobal.addImagesForSlider()
        slidePic()

        featuredToday()

        val fragmentContent = findNavController(R.id.fragment)
        bottomnav.setupWithNavController(fragmentContent)




        getJson().execute(DataGlobal.jsonUrl)

        for (m in DataGlobal.moviesList) {
            println("slika je" + m.image)
        }

        // val slikaUrl = DataGlobal.moviesList[6]
        //  val slikaOkvir = findViewById<ImageView>(R.id.bigPictture)


        // println("slike su" + getPicFromJson(slikaOkvir).execute(slikaUrl.toString()))


    }

    inner class getJson : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val conn = URL(params[0]).openConnection() as HttpURLConnection

            try {
                conn.connect()
                json = conn.inputStream.use {
                    it.reader().use { reader ->
                        reader.readText()
                    }
                }
            } finally {
                conn.disconnect()
            }
            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            println(result)

            printDataJson(result!!)


        }
    }

    inner class getPicFromJson() : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val url = params[0]

            for (p in params) {
                println("P0:" + p)
            }
            try {
                val imageFromUrl = java.net.URL(url).openStream()
                image = BitmapFactory.decodeStream(imageFromUrl)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)
            DataGlobal.imageList.add(result!!)

        }

    }

    fun printDataJson(getJson: String) {


        val jsonObjekat = JSONObject(getJson)

        // main object

        val errorMessage = jsonObjekat.getString("errorMessage")

        val itemsList = jsonObjekat.getJSONArray("items")

        itemsList.let {
            (0 until it.length()).forEach {
                val movieObject = itemsList.getJSONObject(it)
                val id = movieObject.getString("id")
                val rank = movieObject.getString("rank")
                val rankUpDown = movieObject.getString("rankUpDown")
                val title = movieObject.getString("title")
                val fullTitle = movieObject.getString("fullTitle")
                val year = movieObject.getString("year")
                val image = movieObject.getString("image")
                val crew = movieObject.getString("crew")
                val imdbRating = movieObject.getString("imDbRating")
                val imdbRatingCount = movieObject.getString("imDbRatingCount")


                val imageUrl = getPicFromJson().execute(image)
                println("Slika" + imageUrl.toString())


                val movies = Movies(
                    id,
                    rank,
                    rankUpDown,
                    title,
                    fullTitle,
                    year,
                    image,
                    crew,
                    imdbRating,
                    imdbRatingCount
                )


                DataGlobal.moviesList.add(movies)


            }
        }


    }

    fun featuredToday() {

        bestshowsPrva.setImageResource(R.drawable.strangerthings)
        bestshowsDruga.setImageResource(R.drawable.got)
        trecaSlika.setImageResource(R.drawable.dark)

        prvaSlikaNewMovies.setImageResource(R.drawable.lupin)
        drugaSlikaNew.setImageResource(R.drawable.trapped)
        trecaslikaNewMoview.setImageResource(R.drawable.fauda)

        prvaslikaUpcoming.setImageResource(R.drawable.black)
        drugaSlikaUpcoming.setImageResource(R.drawable.flash)
        trecaSlikaUpcoming.setImageResource(R.drawable.suicide)

    }


    fun slidePic() {

        val bigPic = findViewById<ImageView>(R.id.bigImage)
        val smallPic = findViewById<ImageView>(R.id.smallImage)
        val title = findViewById<TextView>(R.id.movieTitle)
        if (!onpause) {
            DataGlobal.imagesForSlide = arrayListOf()
            DataGlobal.addImagesForSlider()
            if (DataGlobal.imagesForSlide.isEmpty()) {
                Log.d("niz", "Niz je prazan")
            } else {

                if (imageCounter == DataGlobal.imagesForSlide.size) {
                    imageCounter = 0
                }
                val imgMain = DataGlobal.imagesForSlide[imageCounter]
                if (bigPic != null && smallPic != null && title != null) {
                    bigPic.setImageResource(imgMain.imageBig)
                    smallPic.setImageResource(imgMain.imageSmall)
                    title.text = imgMain.title

                }

            }
            Handler(Looper.getMainLooper()).postDelayed({
                imageCounter += 1
                slidePic()
            }, DataGlobal.delayMillis)
        }
    }
}