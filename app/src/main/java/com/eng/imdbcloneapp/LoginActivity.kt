package com.eng.imdbcloneapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        val log = Login()
        loginDugme.setOnClickListener {
            if (textEmail.text.toString() == log.user && textPass.text.toString() == log.password) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(applicationContext,"Wrong username or password", Toast.LENGTH_SHORT).show()
            }

        }
    }
}