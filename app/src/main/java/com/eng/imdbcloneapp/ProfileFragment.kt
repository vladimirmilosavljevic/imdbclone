package com.eng.imdbcloneapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController


class ProfileFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)

        val dugme = view.findViewById<Button>(R.id.signinDugme)
        dugme.setOnClickListener {
            Navigation.findNavController(view)
                .navigate(R.id.action_profileFragment_to_signin_fragment)
        }
        return view
    }


}