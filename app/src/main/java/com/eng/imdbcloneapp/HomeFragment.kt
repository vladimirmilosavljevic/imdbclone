package com.eng.imdbcloneapp

import android.os.Bundle
import android.view.GestureDetector
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import kotlinx.android.synthetic.main.fragment_home.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {

    var m = MainActivity()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onResume() {

        bestshowsPrva.setImageResource(R.drawable.strangerthings)
        bestshowsDruga.setImageResource(R.drawable.got)
        trecaSlika.setImageResource(R.drawable.dark)

        prvaSlikaNewMovies.setImageResource(R.drawable.lupin)
        drugaSlikaNew.setImageResource(R.drawable.trapped)
        trecaslikaNewMoview.setImageResource(R.drawable.fauda)

        prvaslikaUpcoming.setImageResource(R.drawable.black)
        drugaSlikaUpcoming.setImageResource(R.drawable.flash)
        trecaSlikaUpcoming.setImageResource(R.drawable.suicide)
        super.onResume()
    }


}